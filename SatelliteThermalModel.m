clc;
clear;
format long;
%------------------------------------------------------------------------------------------
% INTRODUCTION
% This a tool designed to approximate the temperature of a rectangular prism affixed some distance away from the main satellite.
% The primary purpose of this simulation is to validate the Northern Images Mission Payload of AuroraSat-1.
%------------------------------------------------------------------------------------------


%Assumptions
% 1) The modeled item is a thin rectangular plate whose normal axis aligns with the direction of travel of the satellite bus.
% 2) The shielding of the satellite bus on the plate is not modelled
% 3) The plate has a different reflective / absortion properties for each of it's faces.
% 4) The plate has a uniform specific heat capacity.
% 5) Earth is assumed to have a constant temperature and constant emmisivity

%------------------------------------------------------------------------------------------
%Analysis Parameters
%------------------------------------------------------------------------------------------
  Positive_Coating = 'Rough Al';
  %Positive_Coating = 'Gold Foil';
  %Analysis_Case = 'WinterSolstice';
  Analysis_Case = 'SummerSolstice';
  Special_Case = 'None';
  %Special_Case = 'EdgeLitOnly';
  %Special_Case = 'FrontLitOnly';

%Runtime Configuration
  Time_Step = 30; %seconds
  Orbit_Count = 10;
  Orbit_Period_Min = 92; %min
  Orbit_Period_Sec = Orbit_Period_Min*60;

%Runtime Conditionals
  Sim_Steps = ((Orbit_Period_Sec)/(Time_Step))*Orbit_Count; %Used for step through analysis
  Sim_Duration = Orbit_Count * Orbit_Period_Sec;
  Sim_Interval = Sim_Duration / Sim_Steps;
  Duration = linspace(0,2 * Orbit_Period_Sec); %Used for the plots

%------------------------------------------------------------------------------------------
%Geometric Parameters
%------------------------------------------------------------------------------------------
%Static Values
  Orbit_Inclination = 51.6; %Deg
  Earth_Tilt = 23.5; %Deg
  Earth_Radius = 6378; %km
  Satellite_Orbit_Alt_Starting = 400; %km
  Satellite_Orbit_Alt_Ending = 150; %km

%Screen
  %Geometry Configuration
  Screen_Width = 0.050; % Meters
  Screen_Hieght = 0.050; % Meters
  Screen_Thickness = 0.008; % Meters
  Assembly_Mass = 0.050; %Kilograms
  %Geometry Conditionals
  Planar_Area = Screen_Width * Screen_Hieght; % Meters Squared
  Edge_Area = Screen_Width * Screen_Thickness; % Meters Squared -> Assumption about edge facing

%------------------------------------------------------------------------------------------
%Thermal Properties
%------------------------------------------------------------------------------------------
%Stefan Boltzmann's Constant
  S_Boltzmann_Constant = 0.0000000567; % Watts per square meter and kelvin to the fourth power [W/m2K4]

%Sun Configuration
  Solar_Flux = 1376;% Watts per Meter Squared
  IR_Flux = 231; % Watts per Meter Squared

  %Thermal Configuration
  if(strcmp(Positive_Coating, 'Rough Al'))
    Positive_Screen_Absortivity = 0.5; % 'rough' Aluminum - https://www.engineeringtoolbox.com/radiation-surface-absorptivity-d_1805.html
    Positive_Screen_Emissivity = 0.02; % 'rough' Aluminum - https://www.engineeringtoolbox.com/emissivity-coefficients-d_447.html
  end
  if(strcmp(Positive_Coating,'Gold Foil'))
    Positive_Screen_Absortivity = 0.19; % Gold - https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19840015630.pdf
    Positive_Screen_Emissivity = 0.02; % Gold - https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19840015630.pdf
  end
  Negative_Screen_Absortivity = 0.7; % Place Holder -> Not used yet
  Negative_Screen_Emissivity = 0.2; % Place Holder
  Screen_Specific_Heat_Capacity = 1300; % J per kg Deg-C [J/kgC] - 1300 for FR4
  Initial_Temperature = 193; % Kelvin
  %Thermal Conditionals

%Earth Configuration
  Earth_Emissivity = 1; % Approx from Report
  Earth_Temperature = 255; % Kelvin -> Approximate from Report
  Earth_Albedo_Average = 0.35;

%------------------------------------------------------------------------------------------
%Step Through Analysis
%------------------------------------------------------------------------------------------
% Analysis Case Specific Configuration
if(strcmp(Analysis_Case, 'WinterSolstice') == 1)
  printf('--------------------------------------\n');
  printf('Configuring - Winter Solstice\n');
  printf('--------------------------------------\n');
  %Eclipse Approximator
  Approx_Eclipse_Span = 0; %Deg -> Planar Assumption, actual value is lower.
  Approx_Eclipse_Duration = (Approx_Eclipse_Span / 360) * Orbit_Period_Sec; % Seconds of Ecplise
  Eclipse_Centre = Orbit_Period_Sec / 2;
  Eclipse_Start = Eclipse_Centre - (Approx_Eclipse_Duration / 2);
  Eclipse_Finish = Eclipse_Centre + (Approx_Eclipse_Duration / 2);
  %Min Screen Angle -> Screen is edge lit, satellite is passing through the plane formed by the sun vector and the earths spin axis.
  Min_Screen_Angle = 90; % Angle from normal to sun vector
  %Max Screen Angle -> Screen is oblique to the sun, satellite is at the equator
  Max_Screen_Angle = Orbit_Inclination+Earth_Tilt; % Angle from normal to sun vector
end
if(strcmp(Analysis_Case, 'SummerSolstice') == 1)
  printf('--------------------------------------\n');
  printf('Configuring - Summer Solstice\n');
  printf('--------------------------------------\n');
  %Eclipe Approximator
  Approx_Eclipse_Span = 2*(90-19.8); %Deg
  Approx_Eclipse_Duration = (Approx_Eclipse_Span / 360) * Orbit_Period_Sec; % Seconds of Ecplise
  Eclipse_Centre = Orbit_Period_Sec / 2;
  Eclipse_Start = Eclipse_Centre - (Approx_Eclipse_Duration / 2);
  Eclipse_Finish = Eclipse_Centre + (Approx_Eclipse_Duration / 2);
  %Min Screen Angle -> Screen is edge lit, satellite is passing through the plane formed by the sun vector and the earths spin axis.
  Min_Screen_Angle = 90; % Angle from normal to sun vector
  %Max Screen Angle -> Screen is oblique to the sun, satellite is at the equator
  Max_Screen_Angle = Orbit_Inclination-Earth_Tilt; % Angle from normal to sun vector
end

printf(' * Please Note - Special Case is set to: %s\n',Special_Case);

%Sinusodial Approximator of screen angle
  Sin_Amplitude = (Min_Screen_Angle - Max_Screen_Angle) / 2;
  Sin_Period_Coeff = (2 * pi) / Orbit_Period_Sec;
  Sin_Vertical_Shift = Min_Screen_Angle;
%Initial Settings
  Step_Temperature = Initial_Temperature;
%Limit Tracking
  Max_Temperature = Initial_Temperature;
  Min_Temperature = Initial_Temperature;
  Max_Heat_Gain = 0;
  Max_Heat_Loss = 0;
%Parameter Storage
  Q_Solar_Storage = zeros(Sim_Steps,2);
  Q_Solar_Edge_Storage = zeros(Sim_Steps,2);
  Q_Solar_Positive_Storage = zeros(Sim_Steps,2);
  Q_Out_Storage = zeros(Sim_Steps,2);
  Q_Out_Edge_Storage = zeros(Sim_Steps,2);
  Q_Out_Planar_Positive_Storage = zeros(Sim_Steps,2);
  Q_Out_Planar_Negative_Storage = zeros(Sim_Steps,2);
  Q_Albedo_Storage = zeros(Sim_Steps,2);
  Q_IR_Storage = zeros(Sim_Steps,2);
  Temperature_Storage = zeros(Sim_Steps,2);
  counter = 1;
printf('--------------------------------------\n');
printf('Running Step-Through\n');
printf('--------------------------------------\n');
for (inst_time = 0:Sim_Interval:Sim_Duration)
  Display_Time = inst_time;
  %Instantaneous Screen Angle
  if(strcmp(Special_Case,'None'))
    Screen_Angle = Sin_Amplitude * sin(inst_time * Sin_Period_Coeff) + Sin_Vertical_Shift;
  end
  if(strcmp(Special_Case,'EdgeLitOnly'))
    Screen_Angle = 0;
  end
  if(strcmp(Special_Case,'FrontLitOnly'))
    Screen_Angle = 90;
  end
  %Heat Loss to Deep Space
    Q_Out_Edge = (3 * Edge_Area) * S_Boltzmann_Constant * Positive_Screen_Emissivity * (Step_Temperature ^ 4); % Watts
    Q_Out_Planar_Positive = Planar_Area * S_Boltzmann_Constant * Positive_Screen_Emissivity * (Step_Temperature ^ 4); % Watts
    Q_Out_Planar_Negative = Planar_Area * S_Boltzmann_Constant * Negative_Screen_Emissivity * (Step_Temperature ^ 4); % Watts
  Q_Out = Q_Out_Edge + Q_Out_Planar_Negative + Q_Out_Planar_Positive; % Watts
  %Calculate Energy from Sun
  if(mod(inst_time,Orbit_Period_Sec) < Eclipse_Start || mod(inst_time,Orbit_Period_Sec) > Eclipse_Finish)
    Q_Solar_Edge = Solar_Flux * Edge_Area * abs(cos(Screen_Angle * (pi)/180)) * Positive_Screen_Absortivity;
    Q_Solar_Positive = Solar_Flux * Planar_Area * abs(sin(Screen_Angle * (pi)/180)) * Positive_Screen_Absortivity;
    Q_Solar = Q_Solar_Edge + Q_Solar_Positive;
  else
    Q_Solar_Edge = 0;
    Q_Solar_Positive = 0;
    Q_Solar = 0;
  end
  %Calculate Energy from Earth IR
    Q_IR_Edge = S_Boltzmann_Constant * Positive_Screen_Emissivity * 0.9848 * Edge_Area * ((Step_Temperature ^ 4) - (260^4)); %IR_Flux * Edge_Area * abs(sin(Screen_Angle * (2*pi)/180)) * Positive_Screen_Absortivity;
    Q_IR_Positive = S_Boltzmann_Constant * Positive_Screen_Emissivity * 0.1736 * Planar_Area * ((Step_Temperature ^ 4) - (260^4));%IR_Flux * Planar_Area * abs(cos(Screen_Angle * (2*pi)/180)) * Positive_Screen_Absortivity;
  Q_IR = Q_IR_Edge + Q_IR_Positive;
  %Calculate Albedo from Earth
  Q_Albedo = Q_Solar * Earth_Albedo_Average;
  %Energy Balance
  Q_Net = Q_Solar + Q_Albedo - Q_Out;
  Energy_Net = Q_Net * Sim_Interval;
  %Adjust next step in analysis
  Next_Temperature = Energy_Net / (Assembly_Mass * Screen_Specific_Heat_Capacity) + Step_Temperature;
  Step_Temperature = Next_Temperature;
  %Adjust Temperature Ranges
  if(Step_Temperature < Min_Temperature)
    Min_Temperature = Step_Temperature;
  end
  if(Step_Temperature > Max_Temperature)
    Max_Temperature = Step_Temperature;
  end
  %Adjust Heating Ranges
  if(Q_Net < Max_Heat_Loss)
    Max_Heat_Loss = Q_Net;
  end
  if(Q_Net > Max_Heat_Gain)
    Max_Heat_Gain = Q_Net;
  end
  %Parameter Storage
  Q_Solar_Storage(counter,1) = inst_time;
  Q_Solar_Storage(counter,2) = Q_Solar;
  Q_Solar_Edge_Storage(counter,1) = inst_time;
  Q_Solar_Edge_Storage(counter,2) = Q_Solar_Edge;
  Q_Solar_Positive_Storage(counter,1) = inst_time;
  Q_Solar_Positive_Storage(counter,2) = Q_Solar_Positive;
  Q_Out_Storage(counter,1) = inst_time;
  Q_Out_Storage(counter,2) = Q_Out;
  Q_Out_Edge_Storage(counter,1) = inst_time;
  Q_Out_Edge_Storage(counter,2) = Q_Out_Edge;
  Q_Out_Planar_Positive_Storage(counter,1) = inst_time;
  Q_Out_Planar_Positive_Storage(counter,2) = Q_Out_Planar_Positive;
  Q_Out_Planar_Negative_Storage(counter,1) = inst_time;
  Q_Out_Planar_Negative_Storage(counter,2) = Q_Out_Planar_Negative;
  Q_Albedo_Storage(counter,1) = inst_time;
  Q_Albedo_Storage(counter,2) = Q_Albedo;
  Q_IR_Storage(counter,1) = inst_time;
  Q_IR_Storage(counter,2) = Q_IR;
  Temperature_Storage(counter,1) = inst_time;
  Temperature_Storage(counter,2) = Step_Temperature;
  counter = counter + 1;
end
printf('Temperature varied between %d and %d degrees Celcius during simulation\n', Max_Temperature-273, Min_Temperature-273);
printf('The max heat gain was %d Watts\n', Max_Heat_Gain');
printf('The max heat loss was %d Watts\n', Max_Heat_Loss');
printf('--------------------------------------\n');
printf('Plotting...\n');
printf('--------------------------------------\n');
figure(1);
plot(Q_Solar_Storage(:,1),Q_Solar_Storage(:,2),'-r',Q_Out_Storage(:,1),Q_Out_Storage(:,2),'-b',Q_Albedo_Storage(:,1),Q_Albedo_Storage(:,2),'-g',Q_IR_Storage(:,1),Q_IR_Storage(:,2),'-*');
xlabel('Time - sec');
ylabel('Watts of heating');
figure(2);
plot(Temperature_Storage(:,1),Temperature_Storage(:,2),'-r');
xlabel('Time - sec');
ylabel('Temperature');
figure(3);
plot(Q_Solar_Edge_Storage(:,1),Q_Solar_Edge_Storage(:,2),'-r',Q_Solar_Positive_Storage(:,1),Q_Solar_Positive_Storage(:,2),'-b');
xlabel('Time - sec');
ylabel('Solar Heating Power');
figure(4);
plot(Q_Out_Planar_Positive_Storage(:,1),Q_Out_Planar_Positive_Storage(:,2),'-r',Q_Out_Planar_Negative_Storage(:,1),Q_Out_Planar_Negative_Storage(:,2),'-b', Q_Out_Edge_Storage(:,1),Q_Out_Edge_Storage(:,2),'-g');
xlabel('Time - sec');
ylabel('Radiation Powers Out');
